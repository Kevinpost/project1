"""
  A trivial web server in Python.

  Based largely on https://docs.python.org/3.4/howto/sockets.html
  This trivial implementation is not robust:  We have omitted decent
  error handling and many other things to keep the illustration as simple
  as possible.

This is a basic web server written in Python that is meant to demonstrate understanding of the "handshake" between a client and a server, and then the subsequent transfer of data from the server to the client.

Pageserver is the 'engine' of the server.  It creates a socket to listen for requests (on port 5000 in this case) and waits for requests.  When it receives a request (in this case it only accepts GET requests), it first makes sure the URL is formatted properly (no // or .. or ~ anywhere in the URL).  If it is formatted improperly, it responds with a 403 error. If the request is properly formatted, the server attempts to open the requested file and send the contents to the client.  If the file does not exist, the server responds with a 404 error.

As a bonus, I kept the "CAT" functionality.  If no file is specified, the server will respond with the image of a cat!

This web server is designed to work in "user mode", and thus requires use of a port number above 1000 (I chose 5000).
"""

import os

import config    # Configure from .ini files and command line
import logging   # Better than print statements
logging.basicConfig(format='%(levelname)s:%(message)s',
                    level=logging.INFO)
log = logging.getLogger(__name__)
# Logging level may be overridden by configuration
DOCROOT = "."

import socket    # Basic TCP/IP communication on the internet
import _thread   # Response computation runs concurrently with main program


def listen(portnum):
    """
    Create and listen to a server socket.
    Args:
       portnum: Integer in range 1024-65535; temporary use ports
           should be in range 49152-65535.
    Returns:
       A server socket, unless connection fails (e.g., because
       the port is already in use).
    """
    # Internet, streaming socket
    serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # Bind to port and make accessible from anywhere that has our IP address
    serversocket.bind(('', portnum))
    serversocket.listen(1)    # A real server would have multiple listeners
    return serversocket


def serve(sock, func):
    """
    Respond to connections on sock.
    Args:
       sock:  A server socket, already listening on some port.
       func:  a function that takes a client socket and does something with it
    Returns: nothing
    Effects:
        For each connection, func is called on a client socket connected
        to the connected client, running concurrently in its own thread.
    """
    while True:

        log.info("Attempting to accept a connection on {}".format(sock))
        (clientsocket, address) = sock.accept()
        _thread.start_new_thread(func, (clientsocket,))
##
# Starter version only serves cat pictures. In fact, only a
# particular cat picture.  This one.
##
CAT = """
     ^ ^
   =(   )=
"""

# HTTP response codes, as the strings we will actually send.
# See:  https://en.wikipedia.org/wiki/List_of_HTTP_status_codes
# or    http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html
##
STATUS_OK = "HTTP/1.0 200 OK\n\n"
STATUS_FORBIDDEN = "HTTP/1.0 403 Forbidden\n\n"
STATUS_NOT_FOUND = "HTTP/1.0 404 Not Found\n\n"
STATUS_NOT_IMPLEMENTED = "HTTP/1.0 401 Not Implemented\n\n"




def respond(sock):
    """
    This server responds only to GET requests.  Checks for
    properly formatted URL (no ~, //, or ..)
    If no URL is specified, displays the cat.
    """

    options = get_options()
    sent = 0
    request = sock.recv(1024)  # We accept only short requests
    request = str(request, encoding='utf-8', errors='strict')
    log.info("--- Received request ----")
    log.info("Request was {}\n***\n".format(request))

    parts = request.split()
    source_path = DOCROOT + parts[1]
    if parts[0] == "GET":
        #default case, no file specified.
        if len(parts) > 1 and parts[1] == "/":
            transmit(STATUS_OK, sock)
            transmit(CAT, sock)
        else:
            #source_path = os.path.join(DOCROOT, parts[1])
            #make sure that URL is formatted properly
            file_extension_OK = False
            if parts[1].endswith(".html"):
                file_extension_OK = True
            if parts[1].endswith(".css"):
                file_extension_OK = True
            if ('~' in parts[1]) or ("//" in parts[1]) or (".." in parts[1]) or (file_extension_OK == False):
                #invalid URL:  403 error

                transmit(STATUS_FORBIDDEN, sock)
                transmit("403 Forbidden", sock)
                log.warn("403 Forbidden")
                log.warn("Requested file was {}".format(parts[1]))
            else:
                #Borrowed and modified this try/except portion from the provided spew.py
                try:
                    with open(source_path, 'r', encoding='utf-8') as source:
                        #file opens successfully.  Load page.
                        transmit(STATUS_OK, sock)
                        for line in source:
                            transmit(line, sock)
                        source.close()
                except OSError as error:
                    #file unable to open, but input is formatted properly. 404 error
                    transmit(STATUS_NOT_FOUND, sock)
                    transmit("404 Not Found", sock)
                    log.warn("Failed to open or read file")
                    log.warn("Requested file was {}".format(source_path))
                    log.warn("Exception: {}".format(error))
    else:
        #Have not yet implemented anything but "GET"
        log.debug("Source path: {}".format(source_path))
        log.info("Unhandled request: {}".format(request))
        transmit(STATUS_NOT_IMPLEMENTED, sock)
        transmit("\nI don't handle this request: {}\n".format(request), sock)

    sock.shutdown(socket.SHUT_RDWR)
    sock.close()
    return


def transmit(msg, sock):
    """It might take several sends to get the whole message out"""
    sent = 0
    while sent < len(msg):
        buff = bytes(msg[sent:], encoding="utf-8")
        sent += sock.send(buff)

###
#
# Run from command line
#
###


def get_options():
    """
    Options from command line or configuration file.
    Returns namespace object with option value for port
    """
    # Defaults from configuration files;
    #   on conflict, the last value read has precedence
    options = config.configuration()
    # We want: PORT, DOCROOT, possibly LOGGING
    if options.PORT <= 1000:
        log.warning(("Port {} selected. " +
                         " Ports 0..1000 are reserved \n" +
                         "by the operating system").format(options.port))

    return options


def main():
    global DOCROOT
    options = get_options()
    DOCROOT = options.DOCROOT
    port = options.PORT
    if options.DEBUG:
        log.setLevel(logging.DEBUG)
    sock = listen(port)
    log.info("Listening on port {}".format(port))

    log.info("Socket is {}".format(sock))
    serve(sock, respond)



if __name__ == "__main__":
    main()
