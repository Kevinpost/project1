# README #

## **Author Information**

* Name: Kevin Post

* Contact address: Kpost7@uoregon.edu

## **Program description**

This is a basic web server written in Python that is meant to demonstrate understanding of the "handshake" between a client and a server, and then the subsequent transfer of data from the server to the client.

Pageserver is the 'engine' of the server.  It creates a socket to listen for requests (on port 5000 in this case) and waits for requests.  When it receives a request (in this case it only accepts GET requests), it first makes sure the URL is formatted properly (no // or .. or ~ anywhere in the URL).  If it is formatted improperly, it responds with a 403 error. If the request is properly formatted, the server attempts to open the requested file and send the contents to the client.  If the file does not exist, the server responds with a 404 error.

Furthermore, any request for a file with a file extension other than '.html' or '.css' will be rejected with a 403 error.

As a bonus, I kept the "CAT" functionality.  If no file is specified, the server will respond with the image of a cat!

This web server is designed to work in "user mode", and thus requires use of a port number above 1000 (I chose 5000).

## **Example usage**

To start the server: use commands "make start" or "make run".  

To stop the server: use command "make stop".

In the event that you get the following error:
"OSError: [Errno 98] Address already in use"

Type the following command:  "lsof -i :<portnum>" to get the process that is listening on the port, and kill it using kill <process id>
Alternatively, stop the server and modify the credentials.ini file to use a different port number if that process is important.
